// components
import Answer1 from './components/Answer1';
import Answer2 from './components/Answer2';
import Answer3 from './components/Answer3';
import Answer4 from './components/Answer4';

// styles
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {

  return (
    <>
      <div className="container">
        <div className="row mb-5 justify-content-center">
          <div className="col-lg-6">
            {/* 1. */}
            <Answer1 /> 
          </div>
        </div>
        <div className="row mb-5 justify-content-center">
          <div className="col-lg-6">
            {/* 2. */}
            <Answer2 />
          </div>
        </div>
        <div className="row mb-5 justify-content-center">
          <div className="col-lg-6">
            {/* 3.*/}
            <Answer3 />
          </div>
        </div>
        <div className="row mb-5 justify-content-center">
          <div className="col-lg-6">
            {/* 3.*/}
            <Answer4 />
          </div>
        </div>
      </div>
      

      

      
    </>
  );
}

export default App;
