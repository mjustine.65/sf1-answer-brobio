// main libraries/modules
import { useState } from 'react';

export default function Answer1() {

	// init states/values
	const [ isHidden, setIsHidden ] = useState(() => false)

	// function to handle hiding <Answer 1 />
	function handleHideEffect() {

	    setIsHidden(prevState => !isHidden)
	    return
	};

	return (
		(isHidden === false) ? 
		<>
			<h1>1.) Make Me Vanish</h1>
			<button className="btn btn-primary rounded-pill" onClick={handleHideEffect}>Click To Hide</button>
		</> 
		: <button className="btn btn-primary rounded-pill" onClick={handleHideEffect}>Click To Unhide</button>
	)
}