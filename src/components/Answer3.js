// main libraries/modules
import { useState, useEffect } from 'react';
import { Form } from 'react-bootstrap'

const mockDB = []

export default function Answer3() {

	// init states/values
	const [list, setList] = useState(() => [])
	const [item, setItem] = useState(() => '')
	const [isDisabled, setIsDisabled] = useState(() => true)


	// fn to add item to the mockDB 
	const handleAddItem = (e) => {

		e.preventDefault()

		mockDB.push({title: item})
		setItem(prevValue => '')
		getItems()
		return
	}

	// fn to get the items from the mockDB and pass it to the list
	const getItems = () => {

		let mockID = 0
		setList(prevValue => mockDB.map(item => {

			mockID++
			return(
				<tr key={mockID}>
					<td>{item.title}</td>
				</tr>
			)
		}))
		return
	}

	//side effect for the 'Add' button
	useEffect(() => {

		if (item !== '') {
			setIsDisabled(prevState => false)
		} else {
			setIsDisabled(prevState => true)
		}

	},[item])

	return(
		<>	
			<h1>3.) Add To A List</h1>
			<Form className="mb-3" onSubmit={e => handleAddItem(e)}>
					<Form.Group>
						<Form.Control 
							type="text"
							value={item}
							onChange={e => setItem(e.target.value)}
							className="mb-1" required/>
					</Form.Group>
					{
						(isDisabled) ? <button className="w-100 btn btn-outline-secondary rounded-pill" disabled>Add</button> : <button type="submit" className="w-100 btn btn-success rounded-pill">Add</button>
					}
			</Form>
			
			<table className="table table-striped">
				<thead>
					<tr>
						<th>List Items</th>
					</tr>
				</thead>
				<tbody>
					{list}
				</tbody>
			</table>
		</>
	)
}