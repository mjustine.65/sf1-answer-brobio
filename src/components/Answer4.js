// main libraries/modules
import { useState, useEffect} from 'react'
import { Form } from 'react-bootstrap'

export default function Answer4() {

	// init states/values
	const mockDB = [];
	const [username, setUsername] = useState(() => '')
	const [fullname, setFullname] = useState(() => '')
	const [age, setAge] = useState(() => '')
	const [details, setDetails] = useState(() => [])
	const [isDisabled, setIsDisabled] = useState(() => true)

	// fn to handle form submission
	const handleFormSubmit = (e) => {

		e.preventDefault()

		mockDB.push({

			username: username,
			fullname: fullname,
			age: age
		})

		setUsername(prevValue => '')
		setFullname(prevValue => '')
		setAge(prevValue => '')
		getDetails()
		return
	}


	// fn to get details
	const getDetails = () => {

		let mockID = 0
		setDetails(prevValue => mockDB.map(details => {

			mockID++
			return(
				<tr key={mockID}>
					<td>{details.username}</td>
					<td>{details.fullname}</td>
					<td>{details.age}</td>
				</tr>
			)
		}))
		return
		
	}

	useEffect(() => {

		if (username !== '' && fullname !== '' && age !== '') {

			setIsDisabled(prevState => false)
		} else {

			setIsDisabled(prevState=> true)
		}
		
	}, [username, fullname, age])

	return(
		<>	
			<h1>4.) Submit A Form</h1>
			<Form className="mb-3" onSubmit={e => handleFormSubmit(e)}>
				<Form.Group>
					<Form.Control 
						type="text"
						value={username}
						onChange={e => setUsername(e.target.value)}
						placeholder="Username: "
						className="mb-1" required/>
					<Form.Control 
						type="text"
						value={fullname}
						onChange={e => setFullname(e.target.value)}
						placeholder="Full Name: "
						className="mb-1" required/>
					<Form.Control 
						type="number"
						min = "0"
						value={age}
						onChange={e => setAge(e.target.value)}
						placeholder="Age: "
						className="mb-1" required/>
				</Form.Group>

				{
					(isDisabled) ?
					<button className="btn btn-outline-secondary rounded-pill w-100" type="submit" disabled>Submit</button>
					: 
					<button className="btn btn-primary rounded-pill w-100" type="submit" >Submit</button>
				}
			</Form>

			<table className="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Username</th>
						<th>Fullname</th>
						<th>Age</th>
					</tr>
				</thead>
				<tbody>
					{details}
				</tbody>
			</table>
		</>
	)
}