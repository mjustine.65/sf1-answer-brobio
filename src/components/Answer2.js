// main libraries/modules
import { useState, useEffect } from 'react';

export default function Answer2() {

	// init states/values
	const [hrTens, setHrTens] = useState(() => 0)
	const [hrOnes, setHrOnes] = useState(() => 0)
	const [minTens, setMinTens] = useState(() => 0)
	const [minOnes, setMinOnes] = useState(() => 0)
	const [secTens, setSecTens] = useState(() => 0)
	const [secOnes, setSecOnes] = useState(() => 0)
	const [isRunning, setIsRunning] = useState(() => false)
	const [isPause, setIsPause] = useState(() => true)

	// SECONDS
	const addSecOnes = () => {

		setSecOnes(prevValue => {
		
			if (prevValue === 9) {

				addSecTens()
				return 0
			} else {

				return prevValue + 1
			}
		})
		return
	}

	const addSecTens = () => {

		setSecTens(prevValue => {

			if (prevValue === 5) {

				addMinOnes()
				return 0
			} else {

				return prevValue + 1
			}
		})
		return
	}

	// MINUTES
	const addMinOnes = () => {

		setMinOnes(prevValue => {

			if (prevValue === 9) {

				addMinTens()
				return 0
			} else {

				return prevValue + 1
			}
		})
		return
	}

	const addMinTens = () => {

		setMinTens(prevValue => {

			if (prevValue === 5) {

				addHrOnes()
				return 0
			} else {

				return prevValue + 1
			}
		})
		return
	}

	// HOURS
	const addHrOnes = () => {

		setHrOnes(prevValue => {

			if (prevValue === 9) {

				addHrTens()
				return 0
			} else {

				return prevValue + 1
			}
		})
		return
	}

	const addHrTens = () => {

		setHrTens(prevValue => {

			if (prevValue === 9) {

				return 0
			} else {

				return prevValue + 1
			}
		})
		return
	}

	// fn to call to start or to stop the timer
	function handleStartStopTimer() {

		setIsRunning(prevState => !isRunning)
		setIsPause(prevState => !isPause)
		return
	}

	// fn to reset the timer
	function handleResetTimer() {

		setIsRunning(prevState => false)
		setIsPause(prevState => true)
		setHrTens(prevValue => 0)
		setHrOnes(prevValue => 0)
		setMinTens(prevValue => 0)
		setMinOnes(prevValue => 0)
		setSecTens(prevValue => 0)
		setSecOnes(prevValue => 0)
		return
	}

	useEffect(() => {

		let startInterval;

		if (isRunning && isPause === false) {

			startInterval = setInterval(addSecOnes, 1000)
		} else {

			clearInterval(startInterval)
		}

		return () => {

			clearInterval(startInterval)
		}

	})

	return(
		<>
			<h1>2.) Simple Stopwatch</h1>
			<p style={{fontSize: "33px"}}>
				
				<span>{`${hrTens}${hrOnes}:${minTens}${minOnes}:${secTens}${secOnes}`}</span>
			</p>
				{/* conditional rendering */}
				{
					(isRunning === false && isPause === true) ?
					<button className="btn btn-success rounded-pill me-1" onClick={handleStartStopTimer}>Start</button>
					: <button className="btn btn-danger rounded-pill me-1" onClick={handleStartStopTimer}>Stop</button>
				}
				<button className="btn btn-secondary rounded-pill" onClick={handleResetTimer}>Reset</button>
		</>

	)
}